package nl.gerard.cars.components

import android.app.Application
import nl.gerard.cars.components.app.AppComponent
import nl.gerard.cars.components.app.AppModule
import nl.gerard.cars.components.app.DaggerAppComponent

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 17-06-2018
 */

object Injector {

    lateinit var appComponent: AppComponent

    init {
        initAppComponent()
    }

    /**
     * Initializes the app component with the appComponent
     */
    private fun initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(Application()))
                .build()
    }
}