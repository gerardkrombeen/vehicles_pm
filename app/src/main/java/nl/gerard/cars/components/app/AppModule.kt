package nl.gerard.cars.components.app

import android.app.Application
import android.content.Context
import android.content.res.AssetManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 17-06-2018
 */
@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideApp() = application

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideAssets(): AssetManager = application.assets

}