package nl.gerard.cars.components.api

import io.reactivex.Observable
import nl.gerard.cars.components.Api
import nl.gerard.cars.pojo.VehicleResponse
import retrofit2.HttpException
import retrofit2.Response
import java.io.InvalidObjectException

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 17-06-2018
 */
class Repository(private val api: Api){


    fun getVehicles() :Observable<VehicleResponse> =
        api.getVehicles().map { parseResponse(it)  }

    /**
     * unwraps a response and checks for validity and returns its contents
     * @param response response to be unwrapped
     */
    @Throws(InvalidObjectException::class)
    private fun <T> parseResponse(response: Response<T>): T {
        when {
            response.isSuccessful -> response.body()?.let { return it }
                    ?: throw InvalidObjectException(
                            "Empty response body")
        }
        throw HttpException(response)
    }
}