package nl.gerard.cars.components.app

import dagger.Component
import nl.gerard.cars.components.api.ApiModule
import nl.gerard.cars.components.api.Repository
import javax.inject.Singleton

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 17-06-2018
 */
@Singleton
@Component(modules = [
    AppModule::class,
    ApiModule::class
])
interface AppComponent {
    fun repository(): Repository
}