package nl.gerard.cars.components

import io.reactivex.Observable
import nl.gerard.cars.pojo.VehicleResponse
import retrofit2.Response
import retrofit2.http.GET

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 17-06-2018
 */
interface Api{

    @GET("vehicles")
    fun getVehicles(): Observable<Response<VehicleResponse>>
}