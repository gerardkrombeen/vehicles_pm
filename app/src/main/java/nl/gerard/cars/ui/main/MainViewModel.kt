package nl.gerard.cars.ui.main

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import nl.gerard.cars.components.Injector
import nl.gerard.cars.pojo.Vehicle


/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 12-06-2018
 */
class MainViewModel : ViewModel() {

    private val repository = Injector.appComponent.repository()
    private val disposable = CompositeDisposable()
    private val contentSubject = BehaviorSubject.create<List<Vehicle>>()
    private val errorSubject = BehaviorSubject.create<Throwable>()


    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }

    fun loadContent() {
        disposable.add(repository.getVehicles().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ vehicleResponse ->
                    contentSubject.onNext(vehicleResponse.vehicles)
                }) { t -> errorSubject.onNext(t) })

    }

    @SuppressLint("RxSubscribeOnError")
    fun onContentLoaded(fn: (List<Vehicle>) -> Unit): Disposable = contentSubject
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(fn)

    @SuppressLint("RxSubscribeOnError")
    fun onContentError(fn: (Throwable) -> Unit): Disposable = errorSubject
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(fn)


}