package nl.gerard.cars.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.vehicle_item.view.*
import nl.gerard.cars.R
import nl.gerard.cars.pojo.Vehicle

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 18-06-2018
 */
class MainAdapter(private val context: Context,
        private var items: List<Vehicle>) : RecyclerView.Adapter<MainAdapter.MainItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainItemViewHolder {
        return MainItemViewHolder(LayoutInflater.from(context).inflate(R.layout.vehicle_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MainItemViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class MainItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Vehicle) {
            itemView.car_name.text = item.vrn
        }
    }
}
