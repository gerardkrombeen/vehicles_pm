package nl.gerard.cars.ui.main

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.*
import nl.gerard.cars.R
import nl.gerard.cars.pojo.Vehicle

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 12-06-2018
 */
class MainActivity : AppCompatActivity() {

    private val disposables = CompositeDisposable()

    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(this)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        viewModel.onContentLoaded(::setContent).addTo(disposables)
        viewModel.onContentError(::showError).addTo(disposables)
        viewModel.loadContent()
    }

    override fun onStop() {
        disposables.clear()
        super.onStop()
    }

    private fun setContent(vehicles: List<Vehicle>) {
        main_recyclerview.layoutManager = LinearLayoutManager(this)
        main_recyclerview.adapter = MainAdapter(this, vehicles)
    }

    private fun showError(t: Throwable) {
        Log.e("","")
    }
}