package nl.gerard.cars.ui.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import nl.gerard.cars.R

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
    }
}
