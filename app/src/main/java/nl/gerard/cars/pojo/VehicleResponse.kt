package nl.gerard.cars.pojo

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 17-06-2018
 */
data class VehicleResponse(
        val count: Int,
        val vehicles: List<Vehicle>,
        val currentPage: Int,
        val nextPage: Int,
        val totalPages: Int
)