package nl.gerard.cars.pojo

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 12-06-2018
 */
data class Vehicle(val vehicleId: Int,
        val vrn: String,
        val country: String,
        val color: String,
        val type: String,
        val default: Boolean)
