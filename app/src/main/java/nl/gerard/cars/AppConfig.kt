package nl.gerard.cars

/**
 * @author Gerard Krombeen
 * @version 1.0
 * @since 17-06-2018
 */
object AppConfig {

    val baseUrl = "http://private-6d86b9-vehicles5.apiary-mock.com/"
}
